// AFFIX SCROLL//POSITION FIXED ELEMENTS, 
//perfect for sticking your header or menu to the top of the bar when scrolling
//affix works with the scrolling of the windows, 
//it detects the position of the scroll on the window and then you can set an event listener to that,
//in this case, we detect the height of the header to change the class of the element, 
// we also detect the height of the header plus the height of the content 
//to change the class of the element once the bottom or footer is reached.
(function ($) {   
    window.onscroll = function(){
        var scrollTop = window.pageYOffset; //detect the scrolling position
        var element = document.querySelector('.will-change'); //find the element you want to change classes by tag or id or class, 
        //detect the height of header and top-header or everything before the main content + 30pixels 
        var headerHeight =  document.getElementsByTagName("header")[0].offsetHeight
                        // + document.getElementsByTagName("header").offsetHeight + 30;   you can add more elements to the sum and add padding and margins if you need to    
        // detect height of main content -190pixels for when reaching bottom
        var contentHeight = document.getElementsByTagName("main")[0].offsetHeight -1050;   //1050 this number is there to make the example work, but with defined content it will work     
        if( scrollTop >= (contentHeight) ){          //here i detect any height scroll that i want, 500 in this case,               
            element.classList.add("affix-bottom"); //add class           
            element.classList.remove("affix");         //remove class for desired behavior
        } else if (scrollTop >= headerHeight ){                     
            element.classList.add("affix");
            element.classList.remove("affix-bottom");
        }        
        else{          
            element.classList.remove("affix");  //
            element.classList.remove("affix-bottom");
        }
    }         
})();
